import bisect
import copy
import logging
import math
import random
import errno


from collections import Counter
from operator import itemgetter

import numpy as np
import pandas as pd

from pandas import Series

import scipy
from scipy import stats
from scipy import special
from scipy import ndimage

from heapq import nlargest,nsmallest

from sklearn.feature_selection import SelectKBest,f_classif,mutual_info_classif,chi2
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import ExtraTreesClassifier



from io import open
import os

from beautifultable import BeautifulTable
# from geograpy3 import extraction


ROOT2 = math.sqrt(2)
CEND      = '\33[0m'
CBOLD     = '\33[1m'
CITALIC   = '\33[3m'
CURL      = '\33[4m'
CBLINK    = '\33[5m'
CBLINK2   = '\33[6m'
CSELECTED = '\33[7m'

CBLACK  = '\33[30m'
CRED    = '\33[31m'
CGREEN  = '\33[32m'
CYELLOW = '\33[33m'
CBLUE   = '\33[34m'
CVIOLET = '\33[35m'
CBEIGE  = '\33[36m'
CWHITE  = '\33[37m'

CBLACKBG  = '\33[40m'
CREDBG    = '\33[41m'
CGREENBG  = '\33[42m'
CYELLOWBG = '\33[43m'
CBLUEBG   = '\33[44m'
CVIOLETBG = '\33[45m'
CBEIGEBG  = '\33[46m'
CWHITEBG  = '\33[47m'

CGREY    = '\33[90m'
CRED2    = '\33[91m'
CGREEN2  = '\33[92m'
CYELLOW2 = '\33[93m'
CBLUE2   = '\33[94m'
CVIOLET2 = '\33[95m'
CBEIGE2  = '\33[96m'
CWHITE2  = '\33[97m'

CGREYBG    = '\33[100m'
CREDBG2    = '\33[101m'
CGREENBG2  = '\33[102m'
CYELLOWBG2 = '\33[103m'
CBLUEBG2   = '\33[104m'
CVIOLETBG2 = '\33[105m'
CBEIGEBG2  = '\33[106m'
CWHITEBG2  = '\33[107m'

#currently support only two columns
#column_headers = list of strings
#rows = pandas Series
def TabulateSeriesData(headers,rows):
	table =  BeautifulTable()


	table.column_headers = headers

	cnt = 1

	for i,x in rows.iteritems():
		if(x=="nan"):
			x = "None"
		table.append_row([cnt,i,x])
		cnt+=1
	return table

#datasetname = name of the data set
#dataseturl = associated url
#file = file in which data is being written in
def CreateFileTemplate(datasetname, dataseturl,file):
	if file is None:
		raise TypeError

	file.writelines(datasetname+"\n")
	file.writelines(dataseturl+"\n")
	file.writelines("\n\n\n")

#dataHeader = table's short description
#tabulatedData = a table found after BeautifulTable() is applied
#file = file in which data is being written in
def addDataToFiles(dataHeader,tabulatedData,file):
	if file is None:
		raise TypeError

	file.writelines(dataHeader+"\n\n\n")
	file.write(str(tabulatedData))
	file.writelines("\n\n\n")


def processAndCountSeparatedValues(series,sep):
	data_map = {}

	for i,x in series.iteritems():
		
		t = re.split(pattern=sep,string=str(x))
		t = [x for x in t if x]

		for x in t:
			y = x.strip()
			data_map[y] = data_map.get(y,0)+1

	return Series(data_map)



#df --> dataframe #date_col = string, name of the column  #sep --> string, separator
def split_date(df,date_col_name,sep):
    df = df.copy()
    df['Year']=[d.split(sep)[0] for d in df[date_col_name]]
    df['Month']=[d.split(sep)[1] for d in df[date_col_name]]
    df['Day']=[d.split(sep)[2] for d in df[date_col_name]]
    return df



def lower_(df):
	dff=df.copy()
	cols = list(df.columns)

	for col in cols:
		dff[col] = dff[col].astype(dtype='str')
		dff[col] = dff[col].str.lower()
	return dff


def read_and_clean_csv_files(file_path,encoding="utf-8"):
	df = pd.read_csv(file_path,encoding=encoding)
	df = lower_(df)
	df = df.replace(to_replace=" ",value = "_", regex=True)
	return df


def read_and_clean_xlsx_files(file_path,sheet,skiprows,nrows):
	df = pd.read_excel(file_path,sheet_name = sheet,skiprows = skiprows,nrows=nrows)

	df = lower_(df)
	df = df.replace(to_replace=" ",value = "_", regex=True)

	return df


def find_difference(s1,s2):
	ss1 = set(list(s1))
	ss2 = set(list(s2))
	#ss2 = ss2.union(set(["middle_east","many_countries","west_africa","africa","african_meningitis_belt","asia","central_africa","central_and_eastern_europe","north_caucasus_federal_region_of_the_russian_federation","central_asia","europe","west_bank_and_gaza_strip","latin_america","worldwide","northern_hemisphere","many_countries","region_of_the_americas","south_america","south_east_asia","south_west_indian_ocean","the_great_lakes_region","the_horn_of_africa","western_pacific"]))

	return (ss1 - ss2)

def process_df_year_as_column(df,name):
	#colnames = ["Country","Year",name]
	df_new = pd.DataFrame()
	columns = list(df.columns)
	col_name = os.path.splitext(name)[0]

	print(columns)
	for idx,row in df.iterrows():
		c_name = row[columns[0]]
		print(col_name,idx)
		for i in range(1,len(columns)):
			y_name = columns[i]
			y_val = row[columns[i]]
			#print({"Country":c_name,"Year":y_name,str(col_name):y_val})
			df_new = df_new.append({"Country":c_name,"Year":y_name,str(name):y_val},ignore_index=True)



			#print(y_val," ",end="")
		#print()
	#print()
	#print()
	#print(df_new.head())
	return df_new



# def ExtractCountries(url):
# 	e = extraction.Extractor(url = url)

# 	e.find_entities()

# 	lst = list(set(e.places))

# 	txt = e.text

# 	for x in lst:
# 		print(x)
# 		txt=txt.replace(x,str(CBLUEBG+"{}"+CEND).format(x))
# 		#txt=txt.replace(x,'<span style="color: red">{}</span>'.format(x))
# 	return lst,txt
def max_indices(arr):
    res2 = nlargest(len(arr),enumerate(arr),itemgetter(1))
    return res2

def min_indices(arr):
    res2 = nsmallest(len(arr),enumerate(arr),itemgetter(1))
    return res2


def univariate_anova_f(df):
    cols = list(df.columns)
    array = df.values
    X = array[:,0:(len(cols)-1)]
    Y = array[:,(len(cols)-1)]
    
    test = SelectKBest(score_func=f_classif,k=1)
    fit = test.fit(X,Y)
    
    best_idx = max_indices(fit.scores_)
    
    ret=[]
    
    for x in best_idx:
        ret.append((cols[x[0]],x[1]))
    return ret

def univariate_chi2(df):
    cols = list(df.columns)
    array = df.values
    X = array[:,0:(len(cols)-1)]
    Y = array[:,(len(cols)-1)]
    
    test = SelectKBest(score_func=chi2,k=1)
    fit = test.fit(X,Y)
    
    best_idx = max_indices(fit.scores_)
    
    ret=[]
    
    for x in best_idx:
        ret.append((cols[x[0]],x[1]))
    return ret


def rfe_test(df):
    cols = list(df.columns)
    array = df.values
    X = array[:,0:(len(cols)-1)]
    Y = array[:,(len(cols)-1)]

    model = LogisticRegression()

    rfe = RFE(model)

    fit = rfe.fit(X,Y)

    
    best_idx = min_indices(fit.ranking_)
    
    ret=[]
    
    for x in best_idx:
        ret.append((cols[x[0]],x[1]))
    return ret


def feature_importance(df):
    cols = list(df.columns)
    array = df.values
    X = array[:,0:(len(cols)-1)]
    Y = array[:,(len(cols)-1)]


    model = ExtraTreesClassifier(n_estimators=250,random_state=0)
    
    model.fit(X,Y)

    
    best_idx = max_indices(model.feature_importances_)
    
    ret=[]
    
    for x in best_idx:
        ret.append((cols[x[0]],x[1]))
    return ret	




def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise